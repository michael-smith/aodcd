#!/bin/bash

ip=192.168.0.1
stime=10

log() {
	echo $(date '+%Y-%m-%d %H:%M:%S') $1
}

waitForStart() {
	log "Waiting for first contact with '$ip' to get armed."
	while :
	do
		if ping -W2 -c1 $ip > /dev/null 2>&1; then
			log "First contact with '$ip'."
			return
		else
			sleep $stime
		fi
	done
}

checkConnection() {
	log "Action-on-DC is armed."
	exint=0
	while :
	do
		if ! ping -W2 -c1 $ip > /dev/null 2>&1; then
			if [ $exint -eq 0 ]; then
				log "Connection lost to '$ip'."
			fi
			exint=$((exint+1))
		else
			if [ $exint -gt 0 ]; then
				log "Connected to '$ip'."
				exint=0
			fi
		fi

		if [ $exint -eq 3 ]; then
			log "Shutting down system."
			/sbin/shutdown -P now
			return
		fi

		sleep $stime
	done
}

waitForStart
checkConnection
